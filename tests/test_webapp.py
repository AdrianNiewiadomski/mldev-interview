import json
import unittest

import requests


class TestPredict(unittest.TestCase):
    INCORRECT_RESPONSE_TEXT = "The response text is not numeric."
    CORRECT_VALUE_LIST = [{"value": 100}, {"value": 90}, {"value": 80}, {"value": 70}, {"value": 60},
                          {"value": 50}, {"value": 40}, {"value": 30}, {"value": 20}, {"value": 10}]
    CONSTANT_VALUE_LIST = [{"value": 50}, {"value": 50}, {"value": 50}, {"value": 50}, {"value": 50},
                           {"value": 50}, {"value": 50}, {"value": 50}, {"value": 50}, {"value": 50}]

    def test_predict_wrong_url(self):
        response = requests.get("http://127.0.0.1:5000/wrong_url")
        self._check_status_code(response, 404)

    def test_predict_for_correct_value_list(self):
        response = requests.post("http://127.0.0.1:5000/predict", data=json.dumps(self.CORRECT_VALUE_LIST))
        self._check_status_code(response, 200)
        self.assertTrue(TestPredict.is_number(response.text), self.INCORRECT_RESPONSE_TEXT)

    def test_predict_for_constant_value_list(self):
        response = requests.post("http://127.0.0.1:5000/predict", data=json.dumps(self.CONSTANT_VALUE_LIST))
        self._check_status_code(response, 200)
        self.assertTrue(TestPredict.is_number(response.text), self.INCORRECT_RESPONSE_TEXT)

    def test_predict_for_too_long_value_list(self):
        too_long_value_list = self.CONSTANT_VALUE_LIST + self.CONSTANT_VALUE_LIST
        response = requests.post("http://127.0.0.1:5000/predict", data=json.dumps(too_long_value_list))
        self._check_status_code(response, 500)

    def test_predict_for_empty_list(self):
        response = requests.post("http://127.0.0.1:5000/predict", data=json.dumps([]))
        self._check_status_code(response, 500)

    def test_predict_get(self):
        response = requests.get("http://127.0.0.1:5000/predict")
        self._check_status_code(response, 405)

    def test_metrics(self):
        response = requests.get("http://127.0.0.1:5000/metrics")
        self._check_status_code(response, 200)
        self.assertTrue(TestPredict.is_number(response.text), self.INCORRECT_RESPONSE_TEXT)

    def _check_status_code(self, response, code):
        self.assertTrue(response.status_code == code,
                        f"The response should return status code {code} but was {response.status_code}.")

    @staticmethod
    def is_number(s):
        try:
            float(s)
            return True
        except ValueError:
            return False
