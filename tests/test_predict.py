import logging
import unittest
from datetime import datetime
from unittest.mock import patch

import numpy as np
from traffic_predictor.predictor import Predictor
from xgboost.core import XGBoostError

logging.basicConfig(level=logging.INFO)


class TestPredict(unittest.TestCase):
    def test_load_model(self) -> None:
        with patch.object(Predictor, "_load_model"):
            predictor = Predictor()
        self.assertTrue(predictor._load_model("/prediction.model"), "The model has not been loaded.")

    def test_load_model_bad_path(self) -> None:
        with patch.object(Predictor, "_load_model"):
            predictor = Predictor()
        self.assertFalse(predictor._load_model("bad_path"), "The model has been loaded but shouldn't.")

    def test_predict(self) -> None:
        predictor = Predictor()
        prediction = predictor.predict(datetime.now(), [i * 10 for i in range(10)])
        self.assertIsInstance(prediction, np.float32)

    def test_predict_empty_list(self) -> None:
        predictor = Predictor()
        with self.assertRaises(ValueError) as context:
            predictor.predict(datetime.now(), [])

        self.assertTrue("Model expects exactly 10 last reads but got 0." == str(context.exception))

    def test_predict_model_not_loaded(self) -> None:
        predictor = Predictor()
        predictor.is_model_loaded = False

        with self.assertRaises(XGBoostError) as context:
            predictor.predict(datetime.now(), [])

        self.assertTrue("The model has not been loaded." == str(context.exception))
