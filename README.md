## To create a runnable docker container use:
sudo docker build --tag mldev-interview .

## To run webapp.py without docker:
Create and activate a virtual environment, e.g.:

>mkvirtualenv mldev-interview
> 
>add2virtualenv path/to/your/ml/interview 
 
Then, install flask, numpy and xgboost with:
>pip install -r requirements.txt

And run the app with:
>python3 traffic_predictor/webapp.py