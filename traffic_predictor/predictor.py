import logging
import os

import numpy as np
from xgboost import XGBRegressor
from xgboost.core import XGBoostError

logging.basicConfig(level=logging.INFO)


class Predictor:
    PROJECT_PATH = os.path.realpath(__file__)[:os.path.realpath(__file__).rfind("/")]

    def __init__(self):
        logging.info("Creating predictor.")
        self.xgb_r = XGBRegressor()
        self.is_model_loaded = self._load_model("/prediction.model")

    def _load_model(self, path_to_model):
        try:
            self.xgb_r.load_model(self.PROJECT_PATH + path_to_model)
            logging.info("The model has been loaded correctly.")
            return True
        except XGBoostError:
            logging.error("An error has occurred during an attempt to load the model.")
            return False

    def predict(self, date, last_values):
        if self.is_model_loaded:
            if len(last_values) == 10:
                formatted_date = [date.day, date.hour, date.minute, date.weekday()]

                x = np.array([formatted_date + last_values])
                logging.info(f"Performig prediction for: {x}")
                return self.xgb_r.predict(x)[0]
            else:
                logging.error(f"Model expects exactly 10 last reads but got {len(last_values)}.")
                raise ValueError(f"Model expects exactly 10 last reads but got {len(last_values)}.")
        else:
            logging.error("Unable to perform predition. The model has not been loaded.")
            raise XGBoostError("The model has not been loaded.")
