import json
import logging
import time
from datetime import datetime

from flask import Flask
from flask import request
from predictor import Predictor
from prometheus_client import Counter

logging.basicConfig(level=logging.INFO)

START_TIME = time.time()
logging.info(f"application start at {START_TIME}.")
app = Flask(__name__)
app.config["DEBUG"] = True

counter = Counter("python_requests_operations_total", "The total number of processed requests")


@app.route("/predict", methods=["POST"])
def predict():
    counter.inc()

    data = json.loads(request.data.decode('utf8'))
    logging.info(f"The following data has been received: {data}")

    values = [row["value"] for row in data]
    predicted_value = Predictor().predict(datetime.now(), values)
    logging.info(f"The predicted value is: {predicted_value}")
    return f"{predicted_value}"


@app.route("/metrics", methods=["GET"])
def metrics():
    """
    This function, as specified, returns a number of requests per second on the /predict endpoint.
    However, in the future, it may be beneficial to consider a change its body to:
    return Response(prometheus_client.generate_latest(counter), mimetype="text/plain")
    """
    return str(counter._value.get() / (time.time() - START_TIME))


@app.errorhandler(404)
def page_not_found(e):
    return f"<h1>404</h1><p>{str(e)}</p>", 404


if __name__ == "__main__":
    app.run()
